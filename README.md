# Summer Research

## Papers

- [Test-Case Reduction via Test-Case Generation](./papers/test-case-reduction-via-generation.pdf)

## Paper notes

### Test-Case Reduction via Test-Case Generation

- internal vs external test-case reduction
  - internal reduces (random) input to generator rather than already generated test-cases
  - via internal reduction is close to impossible to generate syntactically incorrect test-cases
  - external reduction may be faster since it has more information about the generated testcase and thus reason about the reduction better

- shortlex optimization
  - random input sequence $s$ is shortlex-smaller than $t$ if $|s| < |t|$ or $|s| = |t|$ and $s$ is lexicographically smaller than $t$

- Hypotesis reduces does 14 passes:
    1. Six passes that delete contiguous regions of the choice sequence.
    2. A pass that replaces a contiguous region of the choice sequence with a sub-region.
    3. A pass for replacing a contiguous region of the choice sequence with a, possibly shorter zeroed sequence of choices.
    4. Four passes for pure lexicographic reduction.
    5. Three passes for common patterns that require simultaneously lexicographically reducing some parts of the choice sequence while deleting others.

- rose tree model
  - another model for reduction
  - easier to implement
  - may not produce as good results as internal reduction

## Questions

- how do the sizes of findings differ (before and after we implemented reducer)?

## Ways to go / other implementations

### [Proptest](https://github.com/proptest-rs/proptest)

- implement proptest strategy for our JSON type
  - [asked on forums and around the community but nobody answered](https://stackoverflow.com/questions/72986320/how-to-create-json-object-strategy-according-to-a-schema-with-rust-proptest)
- [proptest seems to be abandoned but there is an initiative to create a new maintainance team](https://github.com/proptest-rs/proptest/issues/268#issuecomment-1301545889)

### Own implementation of Hypothesis

- beneficial for deep understanding of the test case reduction
- [minithesis](https://github.com/DRMacIver/minithesis) is a simplified version of the algorithm
- there is a [rust port of minithesis](https://github.com/Rik-de-Kort/minithesis-rust)
  - definitely useful, but I'd like to create my own for better understanding
  - does not have license but I asked author and he is fine with others using it

### [Arbtest](https://github.com/matklad/arbtest)

- there is a library that implements shrinking for types created with [arbitrary](https://github.com/rust-fuzz/arbitrary)
- as for now, we use arbitrary as well for test case generation
- it has a really simple implementation of shrinker
- in case of "emergency" we may fallback to similar solution
- arbtes's shrinker architecture
  - arbitrary creates structured date from instructured input (e.g. list of ints/bytes)
  - all arbtest does is that it tries to shrink this input sequence
  - the idea is the same in hypothesis paper. it is the case of internal reduction but the alrorithm for shrinking input sequence is simplified
  - [it tries to half the sequence several times, then 9/10th it several times and finally it subtracts 1 from the lenght of the input sequence several times](https://github.com/matklad/arbtest/blob/230943946f225ed6c3b0c74db4089d6383523c40/src/lib.rs)
  - this algorithm will work well on simple cases that does not contain nested data (like strings and its but not structures and lists)
  - if we have a structure with two fields and the second one is the interesting one (causes bug). the ideal reduction would be to shink the first field to zero (i.e. beying empty) and minimize the second one as far as we can. but this algorithm will result in minimizing the second one a leaving the first one unchaged. with two fields it might be ok but it gets worse as the structure is bigger/more nested. this will happen because data for fields are generated one after another (sequentially) and the shrinker will not try to increase the sequence or alter it when it creates a sequence that does not cause the bug. it will just jump to another shrinking method (of the three listed above)

## Todo

- ask proptest maintainers for help once they get into the project. now might be too soon

## Implementation ideas

- we are having the most difficulties with implementing a `Strategy` / `Possibility` for hashmap type
  - might be a good idea to treat it as a vector of pairs (key, val), where keys are fixed
